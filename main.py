
import sys
from PySide2.QtWidgets import QApplication
from pathlib import Path

from moodboard.main_window import MainWindow


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()

    print(sys.argv)
    if len(sys.argv) >= 2 :
        window.set_main_file(sys.argv[-1])

    #Path(sys.argv[-1])
    #and
    #print()
    #if Path(sys.argv[-1])

    app.exec_()
