from pathlib import Path

MODULE_DIR = Path(__file__).parent
ICON_DIR = MODULE_DIR/'resources'/'icons'
STYLESHEET = MODULE_DIR.joinpath('stylesheet.css').read_text()
