
from PySide2.QtWidgets import QApplication, QSpacerItem
from PySide2.QtCore import Qt

from moodboard.constants import ICON_DIR
from moodboard.widgets.base_widgets import DockWidget, ToolButton, ToolBar, \
PushButton, ColorButton, VFrame

from functools import partial

class DockTools(DockWidget) :
    def __init__(self, main_window) :
        super().__init__()

        self.app = QApplication.instance()

        self.layout.setContentsMargins(2, 32, 2,0)

        transform_btn = ToolButton(name = 'Transform',
        icon_path = ICON_DIR/'cursor.png', tooltip = 'Transform Tool\nShortcut : T')

        crop_btn = ToolButton(name = 'Crop',
        icon_path = ICON_DIR/'crop.png', tooltip = 'Crop Tool\nShortcut : C')

        text_btn = ToolButton(name = 'Text',
        icon_path = ICON_DIR/'text.png', tooltip = 'Add Text\nShortcut : A')

        arrow_btn = ToolButton(name = 'Arrow',
        icon_path = ICON_DIR/'arrow.png', tooltip = 'Draw Arrow\nShortcut : W')

        erase_btn = ToolButton(name = 'Erase',
        icon_path = ICON_DIR/'erase.png', tooltip = 'Erase\nShortcut : E')

        pen_btn = ToolButton(name = 'Grease Pencil',
        icon_path = ICON_DIR/'pen.png', tooltip = 'Draw Stroke\nShortcut : P')

        tools = [transform_btn, crop_btn, text_btn, pen_btn]

        self.tool_bar = ToolBar(tools)
        self.tool_bar.tool_changed.connect(self.on_tool_changed)

        self.tool_bar.setActive(tools[0])

        self.layout.addWidget(self.tool_bar)


        ## Actions Btn
        btn_add_image = PushButton(self.layout, icon=ICON_DIR/'image.png',
        tooltip='Add Image\nShortcut : I', tag='ToolButton', connect=self.app.scene.add_image)

        self.layout.addItem(QSpacerItem(1,25))

        open_btn = PushButton(self.layout, icon=ICON_DIR/'open.png',
        tooltip='Open File\nShortcut : Ctrl+O', tag='ToolButton', connect=main_window.open_file)

        save_btn = PushButton(self.layout, icon=ICON_DIR/'save.png',
        tooltip='Save\nShortcut : Ctrl+S', tag='ToolButton', connect=main_window.save_file)

        export_img_btn = PushButton(self.layout, icon=ICON_DIR/'export_image.png',
        tooltip='Render Selected items\nShortcut : Ctrl+Shift+I', tag='ToolButton', connect=self.app.scene.export_image)

        self.layout.addItem(QSpacerItem(1,25))

        blue_btn = ColorButton(name = 'Blue Strokes',
        color = (60,148,250),  tooltip = 'Set Strokes to Blue')
        red_btn = ColorButton(name = 'Red Strokes',
        color = (249,97,72),  tooltip = 'Set Strokes to Red')
        green_btn = ColorButton(name = 'Green Strokes',
        color = (147,244,139),  tooltip = 'Set Strokes to Green')
        white_btn = ColorButton(name = 'White Strokes',
        color = (220,225,228),  tooltip = 'Set Strokes to White')
        black_btn = ColorButton(name = 'Black Strokes',
        color = (14,16,19),  tooltip = 'Set Strokes to Black')

        colors = [blue_btn, red_btn, green_btn, white_btn, black_btn]

        self.color_bar = ToolBar(colors)
        self.color_bar.tool_changed.connect(self.on_color_changed)
        self.color_bar.setActive(colors[0])

        self.layout.addWidget(self.color_bar)

        self.layout.addStretch()

        main_window.addDockWidget(Qt.LeftDockWidgetArea, self)


    def on_tool_changed(self, tool_name) :
        self.app.active_tool = tool_name
        self.app.signals.tool_changed.emit(tool_name)


    def on_color_changed(self, tool_name) :
        tool = self.color_bar.widget(tool_name)

        self.app.active_color = tool.color
        self.app.signals.color_changed.emit(tool_name)

        if tool.modifier == Qt.CTRL :
            self.app.scene.set_color(color=tool.color)
