
from PySide2.QtWidgets import QApplication

from moodboard.widgets.base_widgets import PushButton
from moodboard.constants import ICON_DIR


class BtnScreenCapture(PushButton) :
    type = 'GREASE_PENCIL'
    def __init__(self):
        super().__init__(icon=ICON_DIR/'screen.png')

        self.setObjectName('Tool')
