
from PySide2.QtCore import Signal, QSize, Qt
from PySide2.QtGui import QPixmap, QImage, QIcon, QPen, QPainter, QColor, QKeySequence
from PySide2.QtWidgets import QWidget, QBoxLayout, QPushButton, QFrame, \
QDockWidget, QSizePolicy, QWidgetAction, QLabel

from pathlib import Path

class Pen(QPen) :
    def __init__(self, color=(0,0,0), width=0, cosmetic=False) :
        super().__init__()

        if isinstance(color, tuple) :
            color = QColor(*color)

        self.setCosmetic(cosmetic)
        self.setColor(color)
        self.setWidth(width)

class Layout(QBoxLayout) :
    def __init__(self, direction, parent=None) :
        super().__init__(direction)


        self.setContentsMargins(0, 0, 0, 0)
        self.setSpacing(0)

        if parent :
            parent.setLayout(self)


class VLayout(Layout) :
    def __init__(self, parent=None) :
        super().__init__(QBoxLayout.TopToBottom, parent)



class HLayout(Layout):
    def __init__(self, parent=None) :
        super().__init__(QBoxLayout.LeftToRight, parent)


class PushButton(QPushButton):
    def __init__(self, parent=None, text='', icon=None, connect=None, tooltip='', tag='') :
        super().__init__()

        self.setText(text)

        if icon :
            icon = Path(icon).as_posix()
            self.setIcon(Icon(icon))

        if tag :
            self.setObjectName(tag)

        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        self.setAttribute(Qt.WA_LayoutUsesWidgetRect)
        #self.setIconSize(QSize(20,20))

        #self.setFixedSize(4, 48)

        if parent :
            parent.addWidget(self)

        if tooltip :
            self.setToolTip(tooltip)

        if connect :
            self.clicked.connect(connect)


class Frame(QFrame):
    def __init__(self) :
        super().__init__()


class VFrame(Frame) :
    def __init__(self) :
        super().__init__()

        self.layout = VLayout(self)



class Image(QImage):
    def __init__(self, image, alpha) :
        super().__init__(Path(image).as_posix())

        p = QPainter()
        p.begin(self)
        p.setCompositionMode(QPainter.CompositionMode_DestinationIn)
        p.fillRect(self.rect(), QColor(0, 0, 0, alpha))
        p.end()

class Icon(QIcon) :
    def __init__(self, icon_path) :
        super().__init__()

        normal_image = Image(icon_path, alpha=175)
        active_image = Image(icon_path, alpha=250)

        self.normal_pixmap = QPixmap().fromImage(normal_image)
        self.active_pixmap = QPixmap().fromImage(active_image)

        self.addPixmap(self.normal_pixmap, state=QIcon.Off)
        self.addPixmap(self.active_pixmap, state=QIcon.On)

class WidgetAction(QWidgetAction) :
    def __init__(self, menu, icon=None, text='', connect=None) :
        super().__init__(menu)

        widget = PushButton(text=text, icon=icon)
        self.setDefaultWidget(widget)


        menu.addAction(self)

        if connect :
            widget.clicked.connect(connect)

        widget.clicked.connect(menu.close)

class ToolButton(PushButton) :
    def __init__(self, icon_path='', name='', tooltip='') :
        super().__init__(icon=icon_path, tooltip=tooltip)

        self.setObjectName('ToolButton')
        #self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        if icon_path :
            self.setIcon(Icon(icon_path))
        self.setCheckable(True)
        self.setFocusPolicy(Qt.NoFocus)
        self.name = name
        self.modifier = ()

    def mousePressEvent(self, e) :
        self.modifier = e.modifiers()
        self.toggle()
        self.pressed.emit()
        #return

class ColorButton(ToolButton) :
    def __init__(self, color, name='', tooltip='') :
        super().__init__(name=name, tooltip=tooltip)

        self.color = QColor(*color)
        self.setObjectName('ColorButton')

        color = self.color.getRgb()
        self.setStyleSheet(f"background-color : rgba{tuple(color)}")

class ToolBar(VFrame):
    tool_changed = Signal(str)
    def __init__(self, widgets) :
        super().__init__()

        self.layout.setSpacing(5)
        #self.layout.setAlignment(Qt.AlignHCenter)
        self.widgets = []
        self.active_widget = None

        for w in widgets :
            self.addWidget(w)

    def widget(self, name) :
        widgets = [w for w in self.widgets if w.name == name]
        if not widgets :
            print('The widget', widget, 'is not in', self.widgets)
            return
        return widgets[0]

    def addWidget(self, tool_btn) :
        tool_btn.pressed.connect(lambda : self.setActive(tool_btn))
        self.widgets.append(tool_btn)
        self.layout.addWidget(tool_btn)

    def setActive(self, widget) :
        if isinstance(widget, str) :
            widget = self.widget(widget)

        if self.active_widget :
            self.active_widget.setChecked(False)

        self.active_widget = widget
        widget.setChecked(True)

        self.tool_changed.emit(widget.name)
        #print('SetActive')

        #for w in [w for w in self.widgets if w is not widget] :
        #    w.setChecked(False)

class HFrame(Frame):
    def __init__(self) :
        super().__init__()

        self.layout = HLayout(self)


class DockWidget(QDockWidget) :
    def __init__(self) :
        super().__init__()

        self.setTitleBarWidget(QWidget() )
        self.setObjectName('Docker')
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        self.widget = VFrame()

        #self.widget.setAutoFillBackground(False)
        #self.widget.setAttribute(Qt.WA_TranslucentBackground, True)
        self.widget.setWindowFlags(Qt.FramelessWindowHint)

        self.setWidget(self.widget)
        self.widget.setObjectName('Docker')
        self.layout = self.widget.layout

        #self.setAutoFillBackground(False)
        #self.setWindowFlags(Qt.FramelessWindowHint)
        #self.setAttribute(Qt.WA_NoSystemBackground, True)
        #self.setAttribute(Qt.WA_TranslucentBackground, True)
