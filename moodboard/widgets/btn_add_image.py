
from PySide2.QtWidgets import QApplication, QFileDialog

from moodboard.widgets.base_widgets import PushButton
from moodboard.constants import ICON_DIR


class BtnAddImage(PushButton) :
    def __init__(self) :
        super().__init__(icon=ICON_DIR/'image.png', tooltip='Add Image')

        self.app = QApplication.instance()
        self.setObjectName('Tool')
        self.file_dlg = QFileDialog(self)
        self.clicked.connect(lambda : self.file_dlg.exec())
