
from PySide2.QtCore import QRect, QRectF, QPoint, Qt, QLineF, QMarginsF
from PySide2.QtGui import QColor, QKeySequence, QTransform, QMatrix, QPainter, \
QClipboard, QImage
from PySide2.QtWidgets import QApplication, QGraphicsScene, QFileDialog, \
QGraphicsView, QGraphicsPixmapItem, QGraphicsTextItem

from moodboard.constants import ICON_DIR
from moodboard.graphics.tool_transform import ToolTransform
from moodboard.graphics import ItemImage, ItemGreasePencil, ItemText
from moodboard.widgets.base_widgets import PushButton, Pen
from moodboard.widgets.btn_add_image import BtnAddImage
from moodboard.widgets.btn_screen_capture import BtnScreenCapture

import json
from pathlib import Path


class GraphicsScene(QGraphicsScene):
    def __init__(self, view):
        super().__init__(view)

        self.app = QApplication.instance()
        self.view = view
        self.size = [384000, 216000]

        #self.setBackgroundBrush(QColor(34, 36, 39))
        w, h = self.size
        self.setSceneRect( QRect(-w / 2, -h / 2, w, h) )

        self.selectionChanged.connect(self.on_selection_changed)

        self.tool_transform = ToolTransform(self, [])

        self.item_types = ('TEXT', 'IMAGE','GREASE_PENCIL')

        view.setScene(self)
        #self.grease_pencil_tool = GreasePencilTool(self)

        #ItemImage(self, image="D:/STUDIO/dev/python/moodboard/1.jpg" )

        #self.addItem(QGraphicsPixmapItem("D:/STUDIO/dev/python/moodboard/1.jpg"))

    def grid(self, left, right, top, bottom, size) :
        v_lines = [QLineF(x, top, x, bottom) for x in range(left,right,size)]
        h_lines = [QLineF(left, y, right, y) for y in range(top,bottom,size)]

        return v_lines+h_lines


    def drawBackground(self, painter, rect):
        painter.setRenderHint(QPainter.Antialiasing, False)
        painter.setBrush(QColor(39, 41, 44))
        painter.drawRect(rect)

        '''
        step = 750

        left = int(rect.left()) - (int(rect.left()) % step)
        top = int(rect.top()) - (int(rect.top()) % step)

        right = int(rect.right())
        bottom = int(rect.bottom())

        origin_lines = [QLineF(left, 0, right, 0),QLineF(0, top, 0, bottom)]

        painter.setBrush(Qt.NoBrush)

        painter.setPen(Pen(color=(37,39,41), width=1, cosmetic=True))
        painter.drawLines(self.grid(left, right, top, bottom, step))
        '''

        #painter.setPen(Pen(color=(54,56,59), width=1, cosmetic=True))
        #painter.drawLines(origin_lines)

    def on_selection_changed(self) :
        self.tool_transform.set_items(self.selected_items())

    def max_z(self) :
        items = self.items()
        if not items :
            return 0

        #print(max([i.zValue() for i in items])+1)
        return max([i.zValue() for i in items])+1

    def items(self, type=None) :
        types = type if type else self.item_types
        if not isinstance(types, (list, tuple)) :
            types = [types]
        return [i for i in super().items() if i.type in types]

    def selected_items(self, type=None) :
        types = type if type else self.item_types
        if not isinstance(types, (list, tuple)) :
            types = [types]
        return [i for i in self.selectedItems() if i.type in types]

    def show_context_menu(self, pos) :
       self.context_menu.exec_(QPoint(0,0) )

    def load_from_dict(self, data) :
        self.clear()

        for item_data in data['items'] :
            #print('Load Item', item_data)

            if item_data['type'] == 'IMAGE' :
                ItemImage.from_dict(self, item_data)

            elif item_data['type'] == 'TEXT' :
                ItemText.from_dict(self, item_data)

            elif item_data['type'] == 'GREASE_PENCIL' :
                ItemGreasePencil.from_dict(self, item_data)

        self.update()

        self.view.update()

        #print(self.items())

    def save_images(self, dir_path) :
        # Save Images
        for image_item in [i for i in self.items() if i.type =='IMAGE'] :
            print('Check path for saving', image_item.name)

            img_name = image_item.name if image_item.name else 'untitled'

            img_path = (dir_path/img_name).with_suffix('.png')
            if img_path.exists():
                continue

            image = image_item._pixmap.toImage()
            #image.convertToColorSpace(QColorSpace.SRgb)
            print('Saving image to', img_path)
            image.save(str(img_path))


    def export_image(self, path=None, items=[]) :
        if not path :
            path, filter = QFileDialog().getSaveFileName(None, "Export Image To", "", "")
            if not path : return

            path = Path(path).with_suffix('.png')

        print('Export Images')
        if not items :
            items = self.selected_items()

        items_state = [i.isSelected() for i in items]

        rect = QRectF()
        for i in items :
            rect = rect.united(i.sceneBoundingRect())
            i.setSelected(False)

        rect+= QMarginsF(150,150,150,150)

        width = 1600
        height = int(rect.height()/rect.width() * width)

        render = QImage(width, height, QImage.Format_ARGB32_Premultiplied)
        #render.fill(Qt.transparent)
        painter = QPainter(render)
        painter.setRenderHint(QPainter.Antialiasing)
        self.render(painter, source=rect)

        painter.end()

        print('Save Render image to', path)
        render.save(str(path))

        for i, state in zip(items, items_state) :
            i.setSelected(state)

    def to_dict(self) :
        data = {'items' : [i.to_dict() for i in self.items()]}

        return data

    def set_color(self, color, items=[]) :
        if not items :
            items = [i for i in self.selected_items(['GREASE_PENCIL', 'TEXT'])]

        for i in items :
            i.set_color(color)

    def add_image(self) :

        path, filter = QFileDialog().getOpenFileName(None, "Add Image", "", "")

        if not path : return

        ItemImage(self, image=path)

        #self.focusItemChanged.connect(self.set_transform_tool)
        #Connections
        #self.app.signals.tool_changed.connect(self.on_tool_changed)

    def arrange_items(self, to='front') :
        items = self.items()
        selected_items = self.selected_items()

        for index, item in enumerate(sorted(items, key=lambda x : x.zValue() ) ) :
            item.setZValue(index)

        if to == 'front' :
            z = len(items)+1
        elif to == 'back' :
            z = -100

        for index, item in enumerate(sorted(selected_items, key=lambda x : x.zValue() ) ) :
            item.setZValue(z)

    def mousePressEvent(self, e) :
        if e.button() == Qt.RightButton :
            e.accept()
            return

        if self.app.active_tool == 'Grease Pencil' :
            #print('Active Color', self.app.active_color)
            ItemGreasePencil(self, e.scenePos() , color=self.app.active_color)

        elif self.app.active_tool == 'Text' :
            text_item = ItemText(self, pos=e.scenePos() )
            text_item.start_editing()

        else :
            super().mousePressEvent(e)

    def select_all(self) :
        for i in self.items() :
            i.setSelected(True)

    '''
    def on_tool_changed(self, tool_name) :
        if tool_name == 'Grease Pencil' :
            print('GP')
            self.grease_pencil_tool.grabMouse()
        else :
            #pass
            self.grease_pencil_tool.ungrabMouse()
    '''

    def unique_name(self, name, names) :
        i = 1
        new_name = name
        while new_name in names :
            new_name = f'{name}_{i:03d}'
            i += 1
        return new_name

    def keyPressEvent(self, e) :
        super().keyPressEvent(e)

        keys = QKeySequence(e.modifiers()| e.key())

        if isinstance(self.focusItem(), ItemText)  :
            return

        if e.key() in ( Qt.Key_Delete, Qt.Key_Clear, Qt.Key_Backspace, Qt.Key_X) :
            for i in self.selected_items() :
                i.remove()

        elif e.matches(QKeySequence.Paste) :
            image = QClipboard().image()
            image_names = [i.name for i in self.items('IMAGE')]

            name = self.unique_name('from_clipboard', image_names)

            ItemImage(self, image, name=name)

        elif e.matches(QKeySequence.SelectAll) :
            print("Select All")

        #e.accept()

    def clear(self) :
        for i in self.items() :
            i.remove()
