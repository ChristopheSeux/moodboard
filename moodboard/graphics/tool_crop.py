
from PySide2.QtCore import Qt, QPoint, QRectF, QRect
from PySide2.QtGui import QPainterPath, QPen, QColor, QVector2D, QTransform, QPainter
from PySide2.QtWidgets import QApplication, QGraphicsItem


class BaseHandle(QGraphicsItem) :
    def __init__(self, parent):
        super().__init__()

        self.parent = parent

        self.radius = 14
        self.border_width = 3

        self.setParentItem(parent)
        self.setCursor(Qt.SizeAllCursor)

        self._mouse_pos = QPoint()
        self._origin = QPoint()

        self.move_vector = QVector2D()
        self.mouse_vector = QVector2D()

        self.size_vector = QVector2D()

        self.top_left_vectors = []


    def rect_from_point(self, point, radius) :
        rect = QRectF()
        rect.setTopLeft(point-QPoint(radius, radius) )
        rect.setBottomRight(point+QPoint(radius, radius) )

        return rect

    def paint(self, painter, options, widget):
        rect = self.boundingRect()
        painter.setPen(Qt.NoPen)
        painter.drawRect(rect)


    def mousePressEvent(self, e) :
        self._mouse_pos = e.scenePos()
        self._crop = self.parent.item.crop
        self._pos = self.parent.item.pos()
        e.accept()
        self.update()


    def mouseMoveEvent(self, e) :
        origin = QVector2D(self._origin)
        _mouse_pos = QVector2D(self._mouse_pos)
        mouse_pos = QVector2D(e.scenePos() )

        #co = max((mouse_pos-_mouse_pos).x(), (mouse_pos-_mouse_pos).y())
        #y = (mouse_pos-_mouse_pos).y()
        #mouse_pos = QVector2D(y, y)

        mouse_vec = (mouse_pos-origin)
        size_vec = (_mouse_pos-origin)

        mouse_vec = size_vec* (mouse_vec.length()/size_vec.length() )

        for i in self.parent.items :
            _top_left = QVector2D(i._rect.topLeft() )
            _bottom_right = QVector2D(i._rect.bottomRight() )

            top_left = origin + ( mouse_vec*(_top_left-origin) / size_vec )
            bottom_right = origin + ( mouse_vec*(_bottom_right-origin) / size_vec )

            _size_vector = (top_left- bottom_right)
            size_vector = (_top_left- _bottom_right)

            size = _size_vector/size_vector

            scale = QTransform().scale(size.x(), size.y())

            i.setTransform(scale*i._transform)
            i.setPos(top_left.x(), top_left.y())

        self.update()

    def paint(self, painter, options, widget):
        painter.setRenderHint(QPainter.Antialiasing)

        rect = self.boundingRect()
        outline_color = QColor(60,148,250)

        painter.setBrush(Qt.NoBrush)

        pen = QPen(outline_color)
        pen.setWidth(self.border_width/self.scene().view.zoom)
        painter.setPen(pen)

        path = QPainterPath(self.origin() )
        path.lineTo(self.center() )
        path.lineTo(self.end() )


        painter.drawPath(path)

        #path.dra

        #painter.drawRect(rect)
        #painter.drawEllipse(rect.center(), rect.width()/4, rect.height()/4)

'''
class ItemHandleLeft(BaseHandle):
    def __init__(self, item):
        super().__init__(item)

        self.setCursor(Qt.SizeHorCursor)

    def boundingRect(self):
        rect = self.parent.sceneBoundingRect()
        w, h = self.parentSize()
        r = self.radius/self.scene().view.zoom
        return QRectF(rect.bottom, 2*r)

    def mouseMoveEvent(self, e) :
        w = e.scenePos().x()- self._mouse_pos.x()

        self.item.prepareGeometryChange()
        self.item.setSize(width = self._width-w)
        self.item.setX(self._x + w)
'''
'''
class ItemHandleRight(BaseHandle):
    def __init__(self, item):
        super().__init__(item)

        self.setCursor(Qt.SizeHorCursor)

    def boundingRect(self):
        w, h = self.parentSize()
        r = self.radius/self.scene().view.zoom
        return QRectF(w-r, (h/2)-r, 2*r, 2*r)

    def mouseMoveEvent(self, e) :
        h = e.scenePos().x()- self._mouse_pos.x()

        self.item.prepareGeometryChange()
        self.item.setSize(width = self._width+h)
'''
'''
class ItemHandleBottom(BaseHandle):
    def __init__(self, item):
        super().__init__(item)

        self.setCursor(Qt.SizeVerCursor)

    def boundingRect(self):
        w, h = self.parentSize()
        r = self.radius/self.scene().view.zoom
        return QRectF((w/2)-r, h-r, 2*r, 2*r)

    def mouseMoveEvent(self, e) :
        h = e.scenePos().y()- self._mouse_pos.y()

        self.item.prepareGeometryChange()
        self.item.setSize(height = self._height+h)
'''

class ItemHandleBottomLeft(BaseHandle):
    def __init__(self, item):
        super().__init__(item)

        self.setCursor(Qt.SizeBDiagCursor)

    def origin(self) :
        return self.boundingRect().topLeft()

    def center(self):
        return self.boundingRect().bottomLeft()

    def end (self) :
        return self.boundingRect().bottomRight()

    def boundingRect(self):
        pos = self.parent.boundingRect().bottomLeft()
        r = self.radius/self.scene().view.zoom
        b = self.border_width/self.scene().view.zoom/2
        return QRect(pos.x()-b, pos.y()-r+b, r, r)

    def mouseMoveEvent(self, e) :
        w = self._mouse_pos.x() - e.scenePos().x()
        h = self._mouse_pos.y() - e.scenePos().y()
        c = self._crop

        transform = self.parent.item.transform()
        sx = transform.m11()
        sy = transform.m22()
        rect = QRect(c.x()-w/sx, c.y(), c.width()+w/sx, c.height()-h/sy)

        offset_x = w if rect.x()>0 else 0

        self.parent.item.prepareGeometryChange()
        self.parent.item.setX(self._pos.x()-offset_x )
        self.parent.item.set_crop(rect )


class ItemHandleBottomRight(BaseHandle):
    def __init__(self, item):
        super().__init__(item)

        self.setCursor(Qt.SizeFDiagCursor)

    def origin(self) :
        return self.boundingRect().bottomLeft()

    def center(self):
        return self.boundingRect().bottomRight()

    def end (self) :
        return self.boundingRect().topRight()

    def boundingRect(self):
        pos = self.parent.boundingRect().bottomRight()
        r = self.radius/self.scene().view.zoom
        b = self.border_width/self.scene().view.zoom/2

        return QRect(pos.x()-r+b, pos.y()-r+b, r, r)

    def mouseMoveEvent(self, e) :
        w = self._mouse_pos.x() - e.scenePos().x()
        h = self._mouse_pos.y() - e.scenePos().y()
        c = self._crop

        transform = self.parent.item.transform()
        sx = transform.m11()
        sy = transform.m22()
        rect = QRect(c.x(), c.y(), c.width()-w/sx, c.height()-h/sy)

        self.parent.item.set_crop(rect )


class ItemHandleTopRight(BaseHandle):
    def __init__(self, item):
        super().__init__(item)

        self.setCursor(Qt.SizeBDiagCursor)

    def origin(self) :
        return self.boundingRect().topLeft()

    def center(self):
        return self.boundingRect().topRight()

    def end (self) :
        return self.boundingRect().bottomRight()

    def boundingRect(self):
        pos = self.parent.boundingRect().topRight()
        r = self.radius/self.scene().view.zoom
        b = self.border_width/self.scene().view.zoom/2

        return QRect(pos.x()-r+b, pos.y()-b, r, r)

    def mouseMoveEvent(self, e) :
        w = self._mouse_pos.x() - e.scenePos().x()
        h = self._mouse_pos.y() - e.scenePos().y()
        c = self._crop

        transform = self.parent.item.transform()
        sx = transform.m11()
        sy = transform.m22()
        rect = QRect(c.x(), c.y()-h/sy, c.width()-w/sx, c.height()+(h/sy))

        offset_y = h if rect.y()>0 else 0

        self.parent.item.prepareGeometryChange()
        self.parent.item.setY(self._pos.y()-offset_y)
        self.parent.item.set_crop(rect )


class ItemHandleTopLeft(BaseHandle):
    def __init__(self, parent):
        super().__init__(parent)

        self.setCursor(Qt.SizeFDiagCursor)

    def origin(self) :
        return self.boundingRect().bottomLeft()

    def center(self):
        return self.boundingRect().topLeft()

    def end (self) :
        return self.boundingRect().topRight()

    def boundingRect(self):
        pos = self.parent.boundingRect().topLeft()
        r = self.radius/self.scene().view.zoom
        b = self.border_width/self.scene().view.zoom/2

        return QRect(pos.x()-b, pos.y()-b, r, r)

    def mouseMoveEvent(self, e) :
        w = self._mouse_pos.x() - e.scenePos().x()
        h = self._mouse_pos.y() - e.scenePos().y()
        c = self._crop

        transform = self.parent.item.transform()
        sx = transform.m11()
        sy = transform.m22()
        rect = QRect(c.x()-w/sx, c.y()-h/sy, c.width()+w/sx, c.height()+h/sy)

        offset_x = w if rect.x()>0 else 0
        offset_y = h if rect.y()>0 else 0

        self.parent.item.prepareGeometryChange()
        self.parent.item.setPos(self._pos.x()-offset_x, self._pos.y()-offset_y)
        self.parent.item.set_crop(rect )


class ToolCrop(QGraphicsItem) :
    type = 'TOOL_CROP'
    def __init__(self, scene, item):
        super().__init__()

        #self.setFlags(QGraphicsItem.ItemIsMovable)
        self.setAcceptedMouseButtons( Qt.NoButton )
        self.setZValue(1001)

        scene.addItem(self)

        self.setSelected(True)
        self.setVisible(False)

        self.item = item

        #img = QGraphicsPixmapItem(item.pixmap())
        #img.setFlag(QGraphicsItem.ItemStacksBehindParent)
        #img.setOpacity(0.15)
        #img.setParentItem(self)

        self.handle_top_left = ItemHandleTopLeft(self)
        self.handle_top_right = ItemHandleTopRight(self)
        self.handle_bottom_right = ItemHandleBottomRight(self)
        self.handle_bottom_left = ItemHandleBottomLeft(self)

        #self.setVisible(False)
        signals = QApplication.instance().signals
        signals.tool_changed.connect(lambda t : self.setActive(self.item.isSelected()))

    def setActive(self, state=True):
        active_tool = QApplication.instance().active_tool

        if active_tool != 'Crop':
            state = False
        #print(state , self.item.isSelected())
        self.setVisible(state)
        self.activate = state

    def mousePressEvent(self, e) :
        self._mouse_pos = e.scenePos()

    def mouseMoveEvent(self, e) :
        w = e.scenePos().x()- self._mouse_pos.x()
        h = e.scenePos().y()- self._mouse_pos.y()

    def boundingRect(self) :
        rect = self.item.boundingRect()
        rect = self.item.transform().mapRect(rect)

        return rect


    def paint(self, painter, options, widget):
        rect = self.item._pixmap.rect()

        rect = QRect(rect.x()-self.item.crop.x(), rect.y()-self.item.crop.y(), rect.width(), rect.height() )
        rect = self.item.transform().mapRect(rect)


        painter.setOpacity(0.15)
        painter.drawPixmap(rect, self.item._pixmap)
        painter.setOpacity(1)

        rect = self.boundingRect()
        pen_color = QColor(60, 148, 250)
        pen = QPen(pen_color)
        pen.setWidth(0)
        painter.setPen(pen)

        painter.setBrush(Qt.NoBrush)

        painter.drawRect(rect)
