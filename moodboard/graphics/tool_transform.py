
from PySide2.QtCore import Qt, QRectF, QPoint
from PySide2.QtGui import QPainterPath, QPen, QColor, QVector2D, QPainter, QTransform
from PySide2.QtWidgets import QApplication, QGraphicsItem


class BaseHandle(QGraphicsItem) :
    def __init__(self, parent):
        super().__init__()

        self.setParentItem(parent)
        self.parent = parent

        self.radius = 8

        self._mouse_pos = QPoint()
        self._origin = QPoint()

        self.move_vector = QVector2D()
        self.mouse_vector = QVector2D()

        self.size_vector = QVector2D()

        self.top_left_vectors = []


    def rect_from_point(self, point, radius) :
        rect = QRectF()
        rect.setTopLeft(point-QPoint(radius, radius) )
        rect.setBottomRight(point+QPoint(radius, radius) )

        return rect

    def paint(self, painter, options, widget):
        rect = self.boundingRect()
        painter.setPen(Qt.NoPen)
        painter.drawRect(rect)

    def mousePressEvent(self, e) :
        #print('mousePressEvent')
        self._mouse_pos = e.scenePos()

        for item in self.parent.items :
            item._rect = item.sceneBoundingRect()
            item._transform = QTransform(item.transform() )
            item._pivot_point = item.mapToScene(item.transformOriginPoint() )

        e.accept()

    def mouseMoveEvent(self, e) :
        origin = QVector2D(self._origin)
        _mouse_pos = QVector2D(self._mouse_pos)
        mouse_pos = QVector2D(e.scenePos() )

        #co = max((mouse_pos-_mouse_pos).x(), (mouse_pos-_mouse_pos).y())
        #y = (mouse_pos-_mouse_pos).y()
        #mouse_pos = QVector2D(y, y)

        mouse_vec = (mouse_pos-origin)
        size_vec = (_mouse_pos-origin)

        mouse_vec = size_vec* (mouse_vec.length()/size_vec.length() )

        for i in self.parent.items :
            _top_left = QVector2D(i._rect.topLeft() )
            _bottom_right = QVector2D(i._rect.bottomRight() )
            _pivot_point = QVector2D(i._pivot_point)

            #print('_top_left', _top_left)
            #print('_pivot_point', _pivot_point)

            top_left = origin + ( mouse_vec*(_top_left-origin) / size_vec )
            bottom_right = origin + ( mouse_vec*(_bottom_right-origin) / size_vec )
            pivot_point = origin + ( mouse_vec*(_pivot_point-origin) / size_vec )

            _size_vector = (top_left- bottom_right)
            size_vector = (_top_left- _bottom_right)

            size = _size_vector/size_vector

            scale = QTransform().scale(size.x(), size.y())

            i.setTransform(scale*i._transform)
            i.setPos(pivot_point.x(), pivot_point.y())

        self.update()

    def paint(self, painter, options, widget):
        painter.setRenderHint(QPainter.Antialiasing)

        rect = self.boundingRect()
        brush_color = QColor(220,220,220, 50)
        pen_color = QColor(100,100,100)

        painter.setBrush(brush_color)
        painter.setPen(Qt.NoPen)

        painter.drawEllipse(rect.center(), rect.width()/2.5, rect.height()/2.5)

        brush_color = QColor(60,148,250)
        painter.setBrush(brush_color)

        pen = QPen(pen_color)
        pen.setWidth(0)
        painter.setPen(pen)

        painter.drawEllipse(rect.center(), rect.width()/4, rect.height()/4)

'''
class ItemHandleLeft(BaseHandle):
class ItemHandleRight(BaseHandle):
class ItemHandleBottom(BaseHandle):
class ItemHandleTop(BaseHandle):
'''

class ItemHandleBottomLeft(BaseHandle):
    def __init__(self, item):
        super().__init__(item)

        self.setCursor(Qt.SizeBDiagCursor)

    def boundingRect(self):
        pos = self.parent.boundingRect().bottomLeft()
        r = self.radius
        if self.scene() :
            r = r/self.scene().view.zoom
        return self.rect_from_point(pos, r)

    def mousePressEvent(self, e) :
        self._origin = self.parent.sceneBoundingRect().topRight()
        super().mousePressEvent(e)

        self.update()

class ItemHandleBottomRight(BaseHandle):
    def __init__(self, item):
        super().__init__(item)

        self.setCursor(Qt.SizeFDiagCursor)

    def boundingRect(self):
        pos = self.parent.boundingRect().bottomRight()
        r = self.radius
        if self.scene() :
            r = r/self.scene().view.zoom
        return self.rect_from_point(pos, r)

    def mousePressEvent(self, e) :
        self._origin = self.parent.sceneBoundingRect().topLeft()
        super().mousePressEvent(e)

        self.update()

class ItemHandleTopRight(BaseHandle):
    def __init__(self, item):
        super().__init__(item)

        self.setCursor(Qt.SizeBDiagCursor)

    def boundingRect(self):
        pos = self.parent.boundingRect().topRight()
        r = self.radius
        if self.scene() :
            r = r/self.scene().view.zoom
        return self.rect_from_point(pos, r)

    def mousePressEvent(self, e) :
        self._origin = self.parent.sceneBoundingRect().bottomLeft()
        super().mousePressEvent(e)

        self.update()

class ItemHandleTopLeft(BaseHandle):
    def __init__(self, parent):
        super().__init__(parent)

        self.setCursor(Qt.SizeFDiagCursor)

    def boundingRect(self):
        pos = self.parent.boundingRect().topLeft()
        r = self.radius
        if self.scene() :
            r = r/self.scene().view.zoom
        return self.rect_from_point(pos, r)

    def mousePressEvent(self, e) :
        self._origin = self.parent.sceneBoundingRect().bottomRight()
        super().mousePressEvent(e)

        self.update()


class ToolTransform(QGraphicsItem) :
    def __init__(self, scene, items):
        super().__init__()

        #self.setFlags(QGraphicsItem.ItemIsMovable)
        #self.setCursor(Qt.SizeAllCursor)
        self.setAcceptedMouseButtons( Qt.NoButton )
        self.setZValue(100)

        scene.addItem(self)

        self.setSelected(True)

        self.width = 256
        self.height = 256

        self.items = items
        self.boundbox = QRectF()

        self.is_active = False

        #for item in self.items :
        #    item._transform = QTransform(item.transform() )

        self.handle_top_left = ItemHandleTopLeft(self)
        self.handle_top_right = ItemHandleTopRight(self)
        self.handle_bottom_right = ItemHandleBottomRight(self)
        self.handle_bottom_left = ItemHandleBottomLeft(self)
        #self.handle_left = ItemHandleLeft(self)
        #self.handle_right = ItemHandleRight(self)
        #self.handle_bottom = ItemHandleBottom(self)
        #self.handle_top = ItemHandleTop(self)

        self.setVisible(False)

        signals = QApplication.instance().signals
        signals.tool_changed.connect(lambda t : self.setActive(t == 'Transform'))

    def setActive(self, state=True) :
        self.setVisible(state and len(self.items))
        self.activate = state

    def set_items(self, items) :
        active_tool = QApplication.instance().active_tool
        self.items = items

        self.setActive(active_tool == 'Transform')


    def mousePressEvent(self, e) :
        self._mouse_pos = e.scenePos()

        for i in self.items :
            i._pos = i.scenePos()


    def mouseMoveEvent(self, e) :
        w = e.scenePos().x()- self._mouse_pos.x()
        h = e.scenePos().y()- self._mouse_pos.y()

        for i in self.items :
            i.setPos(i._pos+ QPoint(w, h))

    def boundingRect(self) :
        boundbox = QRectF()
        for item in self.items :
            boundbox = boundbox.united(item.sceneBoundingRect() )

        return boundbox

    def setSize(self, width=None, height=None):
        if width :
            self.width = width
        if height :
            self.height = height


    def paint(self, painter, options, widget):
        painter.setRenderHint(QPainter.Antialiasing)

        rect = self.boundingRect()
        outline_color = QColor(60,148,250)

        painter.setBrush(Qt.NoBrush)


        pen = QPen(outline_color)
        pen.setWidth(0)
        painter.setPen(pen)

        painter.drawRect(rect)
