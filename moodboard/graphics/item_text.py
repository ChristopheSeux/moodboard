
from PySide2.QtCore import Qt, QPoint, QPointF
from PySide2.QtGui import QTransform, QColor, QCursor
from PySide2.QtWidgets import QApplication, QGraphicsTextItem, QGraphicsItem, \
QStyle, QStyleOptionGraphicsItem

from moodboard.widgets.base_widgets import Pen

class ItemText(QGraphicsTextItem) :
    type = 'TEXT'
    def __init__(self, scene, text='', font_size=108, color=None, pos=(0,0), z=None):
        super().__init__(text)

        self.font_size = font_size
        font = self.font()
        font.setPixelSize(font_size)
        self.setFont(font)

        if z is None :
            z = scene.max_z()

        if isinstance(pos, (tuple, list)):
            pos = QPointF(*pos)
        self.setPos(pos)
        self.setZValue(z)
        self.setTextInteractionFlags(Qt.NoTextInteraction)
        self.document().setDocumentMargin(32)
        self.set_color(color)
        self.setFlags(
            QGraphicsItem.ItemIsSelectable|
            QGraphicsItem.ItemIsMovable|
            QGraphicsItem.ItemIsFocusable|
            QGraphicsItem.ItemSendsGeometryChanges)

        scene.addItem(self)



        #self.focusItemChanged.connect(self.set_transform_tool)
        '''
    def focusInEvent(self, e) :
        super().focusInEvent(e)
        self.setSelected(True)

        print('focusIn')
        '''
    def set_color(self, color) :
        if color is None :
            color = (230, 230, 230)
        if not isinstance(color, QColor) :
            color = QColor(*color)

        self.color = color
        self.setDefaultTextColor( self.color )
        #self.update()

    def start_editing(self) :
        self.setTextInteractionFlags(Qt.TextEditorInteraction)
        self.setSelected(True)
        self.setFocus()
        self.setCursor(QCursor(Qt.IBeamCursor))

    def mouseDoubleClickEvent(self, e) :
        self.start_editing()

    def focusOutEvent(self, e) :
        self.setTextInteractionFlags(Qt.NoTextInteraction)
        self.unsetCursor()

        cursor = self.textCursor()
        cursor.clearSelection()
        self.setTextCursor(cursor)

        self.scene().setFocus()

        super().focusOutEvent(e)

        #print('focusOut')

    def mousePressEvent(self, e) :
        super().mousePressEvent(e)

        #print('Press')

    def remove(self) :
        self.scene().removeItem(self)

    #def boundingRect(self) :
    #    return super().boundingRect()+QMarginsF(100,100,100,100)


    def paint(self, painter, options, widget):
        options = QStyleOptionGraphicsItem(options)
        options.state = QStyle.State_None

        super().paint(painter, options, widget)

        if self.isSelected() :
            pen = Pen(color=(100,100,100, 100), width=0)
            painter.setPen(pen)
            painter.setBrush(Qt.NoBrush)
            painter.drawRect(self.boundingRect() )

    @classmethod
    def from_dict(cls, scene, data) :
        self = cls(scene, text=data['text'], pos=data['pos'], color=data.get('color'))

        t = QTransform()
        t.scale(data['scale'], data['scale'])
        self.setTransform(t)

        return self

    def to_dict(self) :
        data = {
            'type' : self.type,
            'pos' : [self.pos().x(), self.pos().y()],
            'text' : self.toPlainText(),
            'scale' : self.transform().m11(),
            'z' : self.zValue(),
            'color' : self.color.getRgb()
        }

        return data
