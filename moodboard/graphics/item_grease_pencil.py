
from PySide2.QtCore import QPointF, Qt, QRectF, QMarginsF
from PySide2.QtGui import QPainterPath, QPainter, QTransform, QColor
from PySide2.QtWidgets import QApplication, QGraphicsItem, QApplication, QGraphicsDropShadowEffect

from moodboard.widgets.base_widgets import Pen


class ItemGreasePencil(QGraphicsItem) :
    type = 'GREASE_PENCIL'
    def __init__(self, scene, pos=(0,0), z=None, color=None):
        super().__init__()

        self.app = QApplication.instance()
        self.drawing = True
        self.radius = 2
        self.stroke = QPainterPath()
        self.strokes = []

        self.set_color(color)

        if z is None :
            z = scene.max_z()

        self.prepareGeometryChange()
        if isinstance(pos, (tuple, list)):
            pos = QPointF(*pos)
        self.setPos(pos)
        self.setZValue(z)
        self.setSelected(True)
        self.setFlags(
            QGraphicsItem.ItemIsSelectable|
            QGraphicsItem.ItemSendsGeometryChanges)

        scene.addItem(self)
        self.grabMouse()

    def set_color(self, color) :
        if color is None :
            color = (60,148,250)
        if not isinstance(color, QColor) :
            color = QColor(*color)

        self.color = color
        self.update()

    def remove(self) :
        self.scene().removeItem(self)

    def boundingRect(self) :
        boundbox = QRectF()
        for stroke in self.strokes :
            boundbox = boundbox.united(stroke.boundingRect() )

        return boundbox

    def paint(self, painter, options, widget):
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(Qt.NoPen)

        pen = Pen(color=self.color, width=self.radius, cosmetic=True)
        painter.setPen(pen)
        painter.setBrush(Qt.NoBrush)

        for stroke in self.strokes :
            painter.drawPath(stroke)

        painter.drawPath(self.stroke)

        if self.isSelected() :
            pen = Pen(color=(255,255,255, 100), width=0 )
            painter.setPen(pen)

            painter.drawRect(self.boundingRect())

    def mousePressEvent(self, e):
        super().mousePressEvent(e)
        #print('press')

    def mouseMoveEvent(self, e):
        super().mouseMoveEvent(e)
        if e.buttons() and Qt.LeftButton and self.drawing:
            self.stroke.lineTo(e.pos())
            self.update()

    def mouseReleaseEvent(self, e):
        super().mouseReleaseEvent(e)
        if e.button() == Qt.LeftButton and self.drawing :
            self.drawing = False
            self.prepareGeometryChange()
            self.strokes.append(self.stroke)


            self.ungrabMouse()
            self.setFlag(QGraphicsItem.ItemIsMovable, True)

    @classmethod
    def from_dict(cls, scene, data) :
        self = cls(scene, pos=data['pos'], z=data['z'], color=data.get('color'))

        for stroke_data in data['strokes'] :
            if not stroke_data : continue
            stroke = QPainterPath()

            x, y = stroke_data[0]
            stroke.moveTo(x, y)

            for point in stroke_data[1:] :
                x, y = point
                stroke.lineTo(x, y)

            self.strokes.append(stroke)

        t = QTransform()
        t.scale(data['scale'], data['scale'])
        self.setTransform(t)

        self.drawing = False
        self.ungrabMouse()
        self.setFlag(QGraphicsItem.ItemIsMovable, True)

        return self

    def to_dict(self) :
        strokes = [[s.elementAt(i) for i in range(s.elementCount())] for s in self.strokes]

        data = {
            'type' : self.type,
            'pos' : [self.pos().x(), self.pos().y()],
            'scale' : self.transform().m11(),
            'strokes' : [[[p.x, p.y] for p in s] for s in strokes],
            'z' : self.zValue(),
            'color' : self.color.getRgb()
        }

        return data
