
from PySide2.QtCore import QRect, QPoint, QPointF, Qt
from PySide2.QtGui import QTransform, QPixmap, QImage

try :
    from PySide2.QtGui import QColorSpace
except :
    QColorSpace = None
    print("The Qt version < 5.12 won't correct immage Colorspace")

from PySide2.QtWidgets import QApplication, QGraphicsPixmapItem, QStyle, QStyleOptionGraphicsItem, QGraphicsItem

from moodboard.graphics.tool_crop import ToolCrop
from moodboard.widgets.base_widgets import Pen

from pathlib import Path


class ItemImage(QGraphicsPixmapItem) :
    type = 'IMAGE'
    def __init__(self, scene, image, name='', pos=(0,0), z=None):
        super().__init__()

        if not isinstance(image, QImage) :
            image_path = Path(image)
            name = image_path.with_suffix('').name
            image = QImage(image_path.as_posix() )

        if not name :
            image_names = [i.name for i in scene.items('IMAGE')]
            name = scene.unique_name('untitled', image_names)

        if isinstance(pos, (tuple, list)):
            pos = QPointF(*pos)

        if z is None :
            z = scene.max_z()

        if QColorSpace :
            image.convertToColorSpace(QColorSpace.SRgb)
        self._pixmap = QPixmap.fromImage(image)

        #self.name = self.unique_name(name, [i.name for i in scene.items('IMAGE')])
        self.name = name
        self.crop_tool = ToolCrop(scene, self)
        self.crop = QRect(0, 0, self._pixmap.width(), self._pixmap.height())
        self._pos = QPoint()
        self._transform = QTransform()

        self.setPos(pos)
        self.setZValue(z)
        self.setPixmap(self._pixmap)
        self.setShapeMode(QGraphicsPixmapItem.BoundingRectShape)
        self.setFlags(
            QGraphicsItem.ItemIsSelectable|
            QGraphicsItem.ItemIsMovable|
            QGraphicsItem.ItemSendsGeometryChanges)

        scene.addItem(self)


    @classmethod
    def from_dict(cls, scene, data) :

        current_dir = QApplication.instance().file.parent
        img_path = (current_dir/data['name']).with_suffix('.png')

        if not img_path.exists() :
            print('The image', img_path, 'not exist')
            return

        self = cls(scene, img_path.as_posix(),
        name=data['name'],
        pos=data['pos'],
        z=data['z'])

        t = QTransform()
        t.scale(data['scale'], data['scale'])
        self.setTransform(t)
        self.set_crop(QRect(*data['crop']))

        return self

    def to_dict(self) :
        c = self.crop

        data = {
            'type' : self.type,
            'pos' : [self.pos().x(), self.pos().y()],
            'name' : self.name,
            'scale' : self.transform().m11(),
            'crop' : [c.x(), c.y(), c.width(), c.height()],
            'z' : self.zValue()
        }

        return data

    def paint(self, painter, options, widget):
        options = QStyleOptionGraphicsItem(options)
        options.state = QStyle.State_None

        super().paint(painter, options, widget)

        if self.isSelected() :
            pen = Pen(color=(250,250,250, 100), width=0)
            painter.setPen(pen)
            painter.setBrush(Qt.NoBrush)
            painter.drawRect(self.boundingRect() )

    def itemChange(self, change, value) :
        if change == QGraphicsItem.ItemSelectedChange :
            self.crop_tool.setActive(value)

        if change == QGraphicsItem.ItemPositionHasChanged :
            self.crop_tool.setPos(self.pos())

        return value

    def remove(self) :
        self.scene().removeItem(self.crop_tool)
        self.scene().removeItem(self)

    def set_crop(self, rect) :
        rect = QRect(
        sorted([0, rect.x(), self._pixmap.width()-1])[1],
        sorted([0, rect.y(), self._pixmap.height()-1])[1],
        sorted([1, rect.width(), self._pixmap.width()-1])[1],
        sorted([1, rect.height(), self._pixmap.height()-1])[1]
        )
        self.setPixmap(self._pixmap.copy(rect))
        self.crop = rect

    def size(self) :
        width = self.boundingRect().width()
        height = self.boundingRect().height()
        return (width, height)
