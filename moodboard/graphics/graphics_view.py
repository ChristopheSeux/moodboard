
from PySide2.QtCore import Qt, QPoint, QPointF
from PySide2.QtGui import QVector2D, QTransform, QImage, QPainter, QIcon
from PySide2.QtWidgets import QApplication, QGraphicsView, QMenu

from moodboard.widgets.base_widgets import Icon, PushButton, WidgetAction
from moodboard.graphics.graphics_scene import GraphicsScene
from moodboard.graphics.item_image import ItemImage
from moodboard.constants import ICON_DIR

from pathlib import Path
import urllib
from urllib import request
from functools import partial
import re


class GraphicsView(QGraphicsView):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.app = QApplication.instance()

        self.zoom = 1
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setTransformationAnchor ( QGraphicsView.NoAnchor )

        self.setDragMode(QGraphicsView.RubberBandDrag)
        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)

        self.setCacheMode(QGraphicsView.CacheBackground)
        self.setOptimizationFlag(QGraphicsView.DontAdjustForAntialiasing)

        self.setRenderHints( QPainter.Antialiasing |
                             QPainter.TextAntialiasing |
                             QPainter.HighQualityAntialiasing |
                             QPainter.SmoothPixmapTransform )

        GraphicsScene(self)

        self._prev_selection = []
        self._mouse_btn = Qt.NoButton
        self._mouse_start = QPoint()
        self._mouse_type = 'NONE'

        #Connections
        self.app.signals.tool_changed.connect(self.on_tool_changed)

        self.context_menu = QMenu()
        WidgetAction(self.context_menu, icon=ICON_DIR/'move_to_front.png',
        text='Move to Front', connect=partial(self.scene().arrange_items, 'front') )
        WidgetAction(self.context_menu, icon=ICON_DIR/'move_to_back.png',
        text='Move to Back', connect=partial(self.scene().arrange_items, 'back'))
        #WidgetAction(self.context_menu, icon=ICON_DIR/'move_to_back.png', text='Move to Back')

        self.set_zoom(-0.75)

    def to_dict(self) :
        t = self.transform()
        data = {
            'pos' : [self.horizontalScrollBar().value(), self.verticalScrollBar().value()],
            'transform' : [t.m11(), t.m12(),t.m21(),t.m22(),t.dx(),t.dy()],
            'zoom' : self.zoom,
        }
        return data

    def load_from_dict(self, data) :
        self.setTransform( QTransform(*data['transform']) )
        self.zoom = data['zoom']
        self.horizontalScrollBar().setValue(data['pos'][0])
        self.verticalScrollBar().setValue(data['pos'][1])

        self.update()

    def contextMenuEvent(self, e) :

        #action = QWidgetAction(self.context_menu) #self.context_menu.addAction(Icon(ICON_DIR/'move_to_front.png'), 'Move to Front')
        #action.setDefaultWidget(PushButton(icon=ICON_DIR/'move_to_back.png', text='Move to Back'))

        pass#self.context_menu.exec_(e.globalPos())

    def on_tool_changed(self, tool_name) :
        if tool_name == 'Transform' :
            self.setDragMode(QGraphicsView.RubberBandDrag)
        else :
            self.setDragMode(QGraphicsView.NoDrag)

    def mousePressEvent(self, e):
        if e.button() != Qt.MiddleButton :
            super().mousePressEvent(e)
        # Restore Selection
        if e.modifiers() == Qt.ShiftModifier :
            for i in self._prev_selection :
                i.setSelected(True)

        self._prev_selection = self.scene().selected_items()
        self._mouse_btn = e.button()
        self._mouse_type = 'PRESS'
        self._mouse_pos = e.pos()
        self._transform = self.transform()

        if e.button() == Qt.RightButton :
            self.context_menu.show()
            self.context_menu.move(e.globalPos())

    def mouseReleaseEvent(self, e):
        super().mouseReleaseEvent(e)
        self._mouse_type = 'RELEASE'

        if e.modifiers() == Qt.ShiftModifier :
            for i in self._prev_selection :
                i.setSelected(True)


    def mouseMoveEvent(self, e):
        self.setInteractive(True)
        if self._mouse_btn == Qt.MiddleButton and self._mouse_type == 'PRESS':

            self.setInteractive(False)

            _mouse_pos = QVector2D(self._mouse_pos)
            mouse_pos = QVector2D(e.pos() )

            mouse_vec =  (mouse_pos - _mouse_pos)/self.zoom

            translate = QTransform().translate(mouse_vec.x(), mouse_vec.y())
            self.setTransform(translate * self._transform)

        else :
            super().mouseMoveEvent(e)


    def dragEnterEvent(self, e) :
        if e.source() :
            e.ignore()
            return super().dragEnterEvent(e)

        e.accept()

    def dragMoveEvent(self, e) :
        if e.source() :
            e.ignore()
            return super().dragMoveEvent(e)

        e.accept()


    def dropEvent(self, e):
        if e.source() :
            return super().dropEvent(e)

        pos = self.mapToScene(e.pos())

        if not e.mimeData().html() :
            files = [Path(u.toLocalFile() ) for u in e.mimeData().urls()]

            mdb_files = [f for f in files if f.suffix == '.mdb']
            if mdb_files :
                print('Open File', mdb_files[0])
                self.app.main_window.set_main_file(mdb_files[0])
                return

            offset = 0
            for f in files:
                if not f.exists() :
                    print('The file', f, 'not exist')
                    continue

                print('Load local image', f)
                image_item = ItemImage(self.scene(), image=f, name = Path(f).name)
                image_item.setPos(pos+QPoint(0, offset))

                offset+=image_item.boundingRect().height()+20

            return

        url = e.mimeData().text()
        print('Read Url', url)

        has_src = lambda url : re.findall("src=(\'|\")(.*?)(\'|\")", url)
        is_image = lambda url : QImage( request.urlretrieve(url)[0] )

        image = is_image(url)

        if not image :
            print('The url not contain image')
            resource = urllib.request.urlopen(url)
            r =  resource.read().decode(resource.headers.get_content_charset())

            src = has_src(r)
            if src :
                url = src[-1][1]
                try :
                    image = is_image(url)
                except Exception as E:
                    print(E)

        if not image :
            src = has_src(e.mimeData().html() )
            if src :
                url = src[-1][1]
                try :
                    image = is_image(url)
                except Exception as E :
                    print(E)

        if not image :
            image = url

        image_name = Path(url).with_suffix('').name
        image_item = ItemImage(self.scene(), image, name=image_name, pos=pos)


    def set_zoom(self, value) :
        zoom = 1+ value

        scale = self.zoom * zoom

        if not 0.025 < scale < 1 : return

        self.zoom = scale

        screen_center = self.mapToScene(self.width()/2, self.height()/2)
        self.scale(zoom, zoom)
        self.centerOn(int(screen_center.x()),int(screen_center.y()))


    def wheelEvent(self, e):
        self.setInteractive(False)
        self.set_zoom(e.angleDelta().y()/1000)
        self.setInteractive(True)
