
from PySide2.QtCore import QObject, Signal, Qt
from PySide2.QtGui import QIcon, QKeySequence
from PySide2.QtWidgets import QApplication, QMainWindow, QFileDialog

from moodboard.constants import STYLESHEET, ICON_DIR
from moodboard.graphics.graphics_view import GraphicsView
from moodboard.widgets.dock_tools import DockTools

from pathlib import Path
import json


class Signals(QObject):
    tool_changed = Signal(str)
    color_changed = Signal(str)


class Context(QObject):
    selected_items = []


class MainWindow(QMainWindow) :
    def __init__(self) :
        super().__init__()

        self.app = QApplication.instance()
        self.app.setStyleSheet(STYLESHEET)

        self.app.signals = Signals()
        self.app.context = Context()
        self.app.active_tool = ''
        self.app.active_color = ''

        view = GraphicsView()
        self.app.view = view
        self.app.scene = view.scene()
        self.app.main_window = self
        self.app.file = ''

        self.setCentralWidget(view)
        self.resize(860, 720)

        self.tools = DockTools(self)

        self.setWindowTitle('Moodboard')
        self.setWindowIcon(QIcon((ICON_DIR/'app_icon.png').as_posix()))

        self.show()

    def to_dict(self) :
        data = {
        'size' : [self.width(), self.height()],
        'pos' : [self.x(), self.y()]
        }
        return data


    def save_file_as(self) :
        path, filter = QFileDialog().getSaveFileName(None, "Save file", "", "(*.mdb)")
        if not path :
            return

        self.app.file = path
        self.save_file()

        print('Save to path : ', path )

    def save_file(self) :
        if not self.app.file :
            self.save_file_as()
            return

        path = Path(self.app.file)

        if path.exists() or path.suffix== '.mdb' :
            dir_path = path.parent
            file_path = path
        else :
            dir_path = path.with_suffix('')
            dir_path.mkdir(exist_ok=True)
            file_path = (dir_path/path.name).with_suffix('.mdb')

        data = {
        'window' : self.to_dict(),
        'view' : self.app.view.to_dict(),
        'scene' : self.app.scene.to_dict()
        }

        file_path.parent.mkdir(exist_ok=True, parents=True)

        data = json.dumps(data, indent=4)
        file_path.write_text(data)

        self.app.scene.save_images(dir_path)

        self.app.scene.export_image(file_path.with_suffix('.png'), self.app.scene.items() )


        self.setWindowTitle('Moodboard' + ' - ' + str(path))

    def keyPressEvent(self, e) :
        super().keyPressEvent(e)

        keys = QKeySequence(e.modifiers()| e.key())

        if e.matches(QKeySequence.StandardKey.Open) :
            self.open_file()
        elif e.matches(QKeySequence.StandardKey.Save) :
            self.save_file()
        elif keys == QKeySequence("Ctrl+Shift+S") :
            self.save_file_as()
        elif keys == QKeySequence("Ctrl+Shift+I") :
            self.app.scene.export_image()
        elif keys == QKeySequence("T") :
            self.tools.tool_bar.setActive('Transform')
        elif keys == QKeySequence("C") :
            self.tools.tool_bar.setActive('Crop')
        elif keys == QKeySequence("A") :
            self.tools.tool_bar.setActive('Text')
        elif keys == QKeySequence("P") :
            self.tools.tool_bar.setActive('Grease Pencil')
        elif keys == QKeySequence("I") :
            self.app.scene.add_image()
        elif keys == QKeySequence("Ctrl+A") :
            self.app.scene.select_all()

    def open_file(self) :
        path, filter = QFileDialog().getOpenFileName(None, "Open File", "", "(*.mdb)")
        if not path :
            return
        self.set_main_file(path)

    def set_main_file(self, path) :
        print('Set main file', path)
        path = Path(path)
        self.setWindowTitle('Moodboard' + ' - ' + str(path))

        self.app.file = path

        if path.exists() :
            data = json.loads(path.read_text())

            self.move(*data['window']['pos'])
            self.resize(*data['window']['size'])

            self.app.view.load_from_dict(data['view'])
            self.app.scene.load_from_dict(data['scene'])
