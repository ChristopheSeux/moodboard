
from pathlib import Path
import subprocess
import imageio
import platform
import os
from os import pathsep

VERSION = '1.2.1'

CURRENT = Path.cwd()/Path(__file__).parent
print('CURRENT', CURRENT)

systems = {'Linux':'linux', 'Darwin':'mac', 'Windows':'windows'}
SYSTEM = systems[platform.system()]
OS_DIR = CURRENT/'build'/SYSTEM

OS_DIR.mkdir(exist_ok=True, parents=True)
os.chdir(OS_DIR)

MAIN_FILE = str(CURRENT/'main.py')
ICON_DIR = str(CURRENT/'moodboard'/'resources'/'icons')

icon = imageio.imread(ICON_DIR+'/app_icon.png')
icon_ext = 'icns' if SYSTEM == 'mac' else 'ico'
ICO = str(CURRENT/'build'/f'app_icon.{icon_ext}')
imageio.imwrite(ICO, icon)

STYLESHEET = str(CURRENT/'moodboard'/'stylesheet.css')


DATA_FILES = [(STYLESHEET, 'moodboard'), (ICON_DIR, 'moodboard/resources/icons')]

#UPX_DIR = str(CURRENT/'upx')

SPEC_PATH = str(OS_DIR)
DIST_DIR = str(OS_DIR/'dist')
BUILD_DIR = str(OS_DIR/'build')

#for f in DATA_FILES :
#    (Path(DIST_DIR)/'Moodboard'/f[1]).mkdir(exist_ok=True, parents=True)

cmd = ['pyinstaller', '-y', '-w', '--name', f'Moodboard_{VERSION}']
if SYSTEM == 'mac' :
    cmd += ['-F']

#cmd += ['--upx-dir', UPX_DIR]
cmd+= ['--specpath', SPEC_PATH, '--distpath', DIST_DIR, '--workpath', BUILD_DIR, '--icon', ICO]

for d in DATA_FILES :
    cmd += ['--add-data', pathsep.join(d)]

cmd+= [MAIN_FILE]

print('cmd', cmd )

subprocess.call(cmd)
